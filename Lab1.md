package it.unimi.di.sweng.lab01;

//import java.util.function.IntPredicate;

public class RomanNumerals {
	
	private int arab[] = {1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000};
	private String roman[] = {"I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"};
	
	public int add(int num, StringBuilder str, int ar, String rom){
		str.append(rom);
		return num-ar;
	}
	
	public String arabicToRoman(int num){
		if(num <= 0)
			throw new IllegalArgumentException();
		StringBuilder str = new StringBuilder();
		int i = arab.length-1;
		while(num >= 0 && i >= 0){
			if(arab[i] <= num)
				num = add(num, str, arab[i], roman[i]);
			else
				i--;
		}return str.toString();
	}
	
	public int index(String rom){
		int l = roman.length-1;
		boolean found = false;
		while(l >= 0 && found == false){
			if(roman[l].compareTo(rom) == 0)
				found = true;
			l--;
		}
		return l+1;
	}
	
	public int romanToArabic(String rom){
		int ar = 0, next = 0, actual = 0;
		if(rom.compareTo("VL") == 0 || rom.compareTo("IIII") == 0 || rom.compareTo("XXXXV") == 0 || rom.compareTo("MMMM") == 0)
			throw new IllegalArgumentException("Invalid Roman numeral");
		for(int i = 0; i < rom.length(); i++){
			actual = index(rom.substring(i, i+1));
			next = -1;
			if(i+2 <= rom.length())
				next = index(rom.substring(i+1, i+2));
			if(actual < next)
				ar -= arab[actual];
			else
				ar += arab[actual];
		}
		return ar;
	}
	
}
